package com.atlassian.prettyurl;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

/**
 */
public class TestingCondition implements Condition {
    String desiredState;

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
        final String param = params.get("desiredState");
        this.desiredState = (param == null || param.isEmpty()) ? "true" : param;
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        if ("exception".equals(desiredState)) {
            throw new RuntimeException("Your wish is my testing command");
        }
        return Boolean.parseBoolean(desiredState);
    }
}
