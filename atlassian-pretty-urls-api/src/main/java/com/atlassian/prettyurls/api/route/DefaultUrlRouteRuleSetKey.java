package com.atlassian.prettyurls.api.route;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

/**
 * A simple implementation based on a string
 *
 * @since 1.11.2
 */
public class DefaultUrlRouteRuleSetKey implements UrlRouteRuleSetKey {
    private final String key;

    public DefaultUrlRouteRuleSetKey(final String key) {
        this.key = Preconditions.checkNotNull(key);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final DefaultUrlRouteRuleSetKey that = (DefaultUrlRouteRuleSetKey) o;

        return key.equals(that.key);
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("key", key)
                .toString();
    }
}
