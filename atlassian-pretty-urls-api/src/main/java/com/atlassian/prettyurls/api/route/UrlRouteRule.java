package com.atlassian.prettyurls.api.route;

import com.google.common.base.MoreObjects;
import com.sun.jersey.api.uri.UriTemplate;

import java.util.List;
import java.util.Objects;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Represents a from and to URI and hence something we can try to match to
 *
 * @since 1.11.2
 */
public class UrlRouteRule {
    /**
     * This controls how the variables inside the paths are passed on to redirected URLs
     */
    public enum ParameterMode {
        PASS_ALL, PASS_UNMAPPED, PASS_NONE
    }

    private final UriTemplate from;
    private final UriTemplate to;
    private final DestinationUrlGenerator toUriGenerator;
    private final ParameterMode parameterMode;
    private final List<String> httpVerbs;
    private final RoutePredicate<UrlRouteRule> predicate;

    public UrlRouteRule(
            final UriTemplate from,
            final UriTemplate to,
            final List<String> httpVerbs,
            final ParameterMode parameterMode) {
        this(from, to, httpVerbs, parameterMode, RoutePredicates.<UrlRouteRule>alwaysTrue());
    }

    public UrlRouteRule(
            final UriTemplate from,
            final UriTemplate to,
            final List<String> httpVerbs,
            final ParameterMode parameterMode,
            final RoutePredicate<UrlRouteRule> predicate) {
        this(from, to, getDefaultToUriGenerator(to), httpVerbs, parameterMode, predicate);
    }

    public UrlRouteRule(
            final UriTemplate from,
            final DestinationUrlGenerator toUriGenerator,
            final List<String> httpVerbs,
            final ParameterMode parameterMode) {
        this(from, toUriGenerator, httpVerbs, parameterMode, RoutePredicates.<UrlRouteRule>alwaysTrue());
    }

    public UrlRouteRule(
            final UriTemplate from,
            final DestinationUrlGenerator toUriGenerator,
            final List<String> httpVerbs,
            final ParameterMode parameterMode,
            final RoutePredicate<UrlRouteRule> predicate) {
        this(from, null, toUriGenerator, httpVerbs, parameterMode, predicate);
    }

    private UrlRouteRule(
            final UriTemplate from,
            final UriTemplate to,
            final DestinationUrlGenerator toUriGenerator,
            final List<String> httpVerbs,
            final ParameterMode parameterMode,
            final RoutePredicate<UrlRouteRule> predicate) {
        this.from = checkNotNull(from);
        this.to = to;
        this.toUriGenerator = checkNotNull(toUriGenerator);
        this.httpVerbs = checkNotNull(httpVerbs);
        this.parameterMode = checkNotNull(parameterMode);
        this.predicate = checkNotNull(predicate);
    }

    private static DestinationUrlGenerator getDefaultToUriGenerator(final UriTemplate to) {
        checkNotNull(to);
        return (request, variables) -> to.createURI(variables);
    }

    public UriTemplate getFrom() {
        return from;
    }

    public UriTemplate getTo() {
        return to;
    }

    public DestinationUrlGenerator getToUriGenerator() {
        return toUriGenerator;
    }

    public ParameterMode getParameterMode() {
        return parameterMode;
    }

    public List<String> getHttpVerbs() {
        return httpVerbs;
    }

    public RoutePredicate<UrlRouteRule> getPredicate() {
        return predicate;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("from", from)
                .add("to", to)
                .add("httpVerbs", httpVerbs)
                .add("parameterMode", parameterMode)
                .add("predicate", predicate)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UrlRouteRule that = (UrlRouteRule) o;
        return Objects.equals(from, that.from) &&
                Objects.equals(to, that.to) &&
                parameterMode == that.parameterMode &&
                Objects.equals(httpVerbs, that.httpVerbs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to, parameterMode, httpVerbs);
    }
}
