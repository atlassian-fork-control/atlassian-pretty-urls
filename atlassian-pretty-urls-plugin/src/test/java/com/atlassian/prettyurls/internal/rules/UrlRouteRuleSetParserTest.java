package com.atlassian.prettyurls.internal.rules;

import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.prettyurls.api.route.RoutePredicate;
import com.atlassian.prettyurls.api.route.UrlRouteRule;
import com.atlassian.prettyurls.api.route.UrlRouteRuleSet;
import com.google.common.collect.Lists;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 */
public class UrlRouteRuleSetParserTest {
    private static final String NO_PATH_INFO = "" +
            "<routing key='pluginkey'>" +
            "   <route />" +
            "</routing>";

    private static final String INVALID_PATH = "" +
            "<routing key='pluginkey' path='/'>" +
            "   <route to='y' />" +
            "</routing>";

    private static final String NO_ROUTE_TO_OR_FROM_ATTR = "" +
            "<routing key='pluginkey' path='path'>" +
            "   <route />" +
            "</routing>";

    private static final String NO_ROUTE_TO_ATTR = "" +
            "<routing key='pluginkey' path='path'>" +
            "   <route from='' />" +
            "</routing>";

    private static final String NO_ROUTE_FROM = "" +
            "<routing key='pluginkey' path='/path'>" +
            "   <route from='' to='y' />" +
            "</routing>";

    private static final String BAD_FROM_URL = "" +
            "<routing key='pluginkey' path='/path'>" +
            "   <route from='/some/bad/url/{badvar : bad[regex\\ ' to='y' />" +
            "</routing>";

    private static final String BAD_TO_URL = "" +
            "<routing key='pluginkey' path='/path'>" +
            "   <route from='x' to='/some/bad/url/{badvar : bad[regex\\' />" +
            "</routing>";

    private static final String SIMPLE_ROUTE = "" +
            "<routing key='pluginkey' path='/path'>" +
            "   <route from='x' to='y' />" +
            "</routing>";

    private static final String REPEATS_WILL_REPEAT = "" +
            "<routing key='pluginkey' path='/path'>" +
            "   <route from='/path/x' to='/path/y' />" +
            "</routing>";


    private static final String VERBS_NONE = "" +
            "<routing key='pluginkey' path='/path'>" +
            "   <route from='x' to='y' />" +
            "</routing>";

    private static final String VERBS_GET = "" +
            "<routing key='pluginkey' path='/path'>" +
            "   <route from='x' to='y' verbs='get' />" +
            "</routing>";

    private static final String VERBS_GET_PUT_DELETE = "" +
            "<routing key='pluginkey' path='/path'>" +
            "   <route from='x' to='y' verbs='get,put,delete' />" +
            "</routing>";

    private static final String VERBS_GET_SUGAR = "" +
            "<routing key='pluginkey' path='/path'>" +
            "   <get from='x' to='y' />" +
            "</routing>";

    private static final String VERBS_POST_SUGAR = "" +
            "<routing key='pluginkey' path='/path'>" +
            "   <post from='x' to='y' />" +
            "</routing>";

    private static final FilterLocation location = FilterLocation.BEFORE_DISPATCH;

    private UrlRouteRuleSetParser parser;

    private MockPredicateMaker predicateMaker = new MockPredicateMaker();

    @Before
    public void setUp() throws Exception {
        parser = new UrlRouteRuleSetParser();
    }

    @Test
    public void testBadParses() throws Exception {
        UrlRouteRuleSet ruleSet;

        ruleSet = parser.parse("moduleKey", loadXML(NO_PATH_INFO), location, predicateMaker);
        assertNull(ruleSet);

        ruleSet = parser.parse("moduleKey", loadXML(INVALID_PATH), location, predicateMaker);
        assertNull(ruleSet);

        ruleSet = parser.parse("moduleKey", loadXML(NO_ROUTE_TO_OR_FROM_ATTR), location, predicateMaker);
        assertEquals("/path", ruleSet.getTopLevelPaths().get(0));
        assertEquals(0, ruleSet.getUrlRouteRules().size());

        ruleSet = parser.parse("moduleKey", loadXML(NO_ROUTE_TO_ATTR), location, predicateMaker);
        assertEquals("/path", ruleSet.getTopLevelPaths().get(0));
        assertEquals(0, ruleSet.getUrlRouteRules().size());

        ruleSet = parser.parse("moduleKey", loadXML(BAD_FROM_URL), location, predicateMaker);
        assertEquals("/path", ruleSet.getTopLevelPaths().get(0));
        assertEquals(0, ruleSet.getUrlRouteRules().size());

        ruleSet = parser.parse("moduleKey", loadXML(BAD_TO_URL), location, predicateMaker);
        assertEquals("/path", ruleSet.getTopLevelPaths().get(0));
        assertEquals(0, ruleSet.getUrlRouteRules().size());
    }

    @Test
    public void testGoodPaths() throws Exception {
        UrlRouteRuleSet ruleSet;

        //
        // We allow from="" so you can map to the top level path directly
        //
        ruleSet = parser.parse("moduleKey", loadXML(NO_ROUTE_FROM), location, predicateMaker);
        assertEquals("/path", ruleSet.getTopLevelPaths().get(0));
        assertEquals(1, ruleSet.getUrlRouteRules().size());
        assertRoute(ruleSet.getUrlRouteRules().get(0), "/path", "/y");

        ruleSet = parser.parse("moduleKey", loadXML(SIMPLE_ROUTE), location, predicateMaker);
        assertEquals("/path", ruleSet.getTopLevelPaths().get(0));
        assertEquals(1, ruleSet.getUrlRouteRules().size());
        assertRoute(ruleSet.getUrlRouteRules().get(0), "/path/x", "/y");

        ruleSet = parser.parse("moduleKey", loadXML(REPEATS_WILL_REPEAT), location, predicateMaker);
        assertEquals("/path", ruleSet.getTopLevelPaths().get(0));
        assertEquals(1, ruleSet.getUrlRouteRules().size());
        assertRoute(ruleSet.getUrlRouteRules().get(0), "/path/path/x", "/path/y");
    }

    @Test
    public void testHttpVerbs() throws Exception {
        UrlRouteRuleSet ruleSet;
        UrlRouteRule rule;

        ruleSet = parser.parse("moduleKey", loadXML(VERBS_NONE), location, predicateMaker);
        rule = ruleSet.getUrlRouteRules().get(0);
        assertEquals(0, rule.getHttpVerbs().size());

        ruleSet = parser.parse("moduleKey", loadXML(VERBS_GET), location, predicateMaker);
        rule = ruleSet.getUrlRouteRules().get(0);
        assertEquals(Lists.newArrayList("GET"), rule.getHttpVerbs());

        ruleSet = parser.parse("moduleKey", loadXML(VERBS_GET_PUT_DELETE), location, predicateMaker);
        rule = ruleSet.getUrlRouteRules().get(0);
        assertEquals(Lists.newArrayList("GET", "PUT", "DELETE"), rule.getHttpVerbs());

    }

    @Test
    public void testSyntacticSugar() throws Exception {
        UrlRouteRuleSet ruleSet;
        UrlRouteRule rule;

        ruleSet = parser.parse("moduleKey", loadXML(VERBS_GET_SUGAR), location, predicateMaker);
        rule = ruleSet.getUrlRouteRules().get(0);
        assertEquals(Lists.newArrayList("GET"), rule.getHttpVerbs());
        assertRoute(rule, "/path/x", "/y");

        ruleSet = parser.parse("moduleKey", loadXML(VERBS_POST_SUGAR), location, predicateMaker);
        rule = ruleSet.getUrlRouteRules().get(0);
        assertEquals(Lists.newArrayList("POST"), rule.getHttpVerbs());
        assertRoute(rule, "/path/x", "/y");

    }

    @Test
    public void testPredicateMaking_when_false() throws Exception {
        UrlRouteRuleSet ruleSet;

        predicateMaker.setDesiredState(false);

        ruleSet = parser.parse("moduleKey", loadXML(SIMPLE_ROUTE), location, predicateMaker);
        assertRoute(ruleSet.getUrlRouteRules().get(0), "/path/x", "/y");
        assertPredicate(ruleSet.getPredicate(), false);

    }

    @Test
    public void testPredicateMaking_when_true() throws Exception {
        UrlRouteRuleSet ruleSet;

        predicateMaker.setDesiredState(true);

        ruleSet = parser.parse("moduleKey", loadXML(SIMPLE_ROUTE), location, predicateMaker);
        assertRoute(ruleSet.getUrlRouteRules().get(0), "/path/x", "/y");
        assertPredicate(ruleSet.getPredicate(), true);

    }

    private void assertPredicate(final RoutePredicate<?> predicate, final boolean state) {
        assertEquals(state, predicate.apply(null, null));
    }

    private void assertRoute(UrlRouteRule urlRouteRule, String from, String to) {
        assertEquals(from, urlRouteRule.getFrom().createURI());
        assertEquals(to, urlRouteRule.getTo().createURI());
    }

    private Element loadXML(String s) throws DocumentException {
        Document document = DocumentHelper.parseText(s);
        return document.getRootElement();
    }
}
