package com.atlassian.prettyurls.internal.rules;

import com.atlassian.prettyurls.api.route.RoutePredicate;
import com.atlassian.prettyurls.api.route.UrlRouteRule;
import com.atlassian.prettyurls.api.route.UrlRouteRuleSet;
import org.dom4j.Element;

import javax.servlet.http.HttpServletRequest;

/**
 */
@SuppressWarnings("unchecked")
public class MockPredicateMaker implements UrlRouteRuleSetParser.PredicateMaker, RoutePredicate {
    boolean desiredState = true;

    public MockPredicateMaker(final boolean desiredState) {
        this.desiredState = desiredState;
    }

    public MockPredicateMaker() {
        this(true);
    }

    public boolean isDesiredState() {
        return desiredState;
    }

    public void setDesiredState(final boolean desiredState) {
        this.desiredState = desiredState;
    }

    @Override
    public RoutePredicate<UrlRouteRuleSet> makeRuleSetPredicate(final Element routing) {
        return this;
    }

    @Override
    public RoutePredicate<UrlRouteRule> makeRulePredicate(final Element route) {
        return this;
    }

    @Override
    public boolean apply(final HttpServletRequest httpServletRequest, final Object route) {
        return desiredState;
    }
}
