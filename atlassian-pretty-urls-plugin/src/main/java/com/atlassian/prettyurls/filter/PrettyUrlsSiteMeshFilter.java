package com.atlassian.prettyurls.filter;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.prettyurls.internal.util.UrlUtils;
import com.atlassian.prettyurls.module.SiteMeshModuleDescriptor;
import com.opensymphony.sitemesh.webapp.SiteMeshFilter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * A filter that performs SiteMesh filtering IF the action has been routed and its asked for SiteMesh to be performed on it
 */
public class PrettyUrlsSiteMeshFilter extends PrettyUrlsCommonFilter {
    private final Filter siteMeshFilterDelegate;
    private final PluginAccessor pluginAccessor;

    public PrettyUrlsSiteMeshFilter(@ComponentImport final PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
        Class<?> siteMeshClass = null;
        try {
            // Some applications such as Stash don't have SiteMesh so we test the waters first to see if its around.
            // if NOT we become a Noop filter
            siteMeshClass = Class.forName("com.opensymphony.sitemesh.webapp.SiteMeshFilter");
        } catch (ClassNotFoundException e) {
        }
        if (siteMeshClass != null) {
            siteMeshFilterDelegate = new SiteMeshFilter();
        } else {
            siteMeshFilterDelegate = null;
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        if (siteMeshFilterDelegate != null) {
            siteMeshFilterDelegate.init(filterConfig);
        }
    }

    @Override
    public void destroy() {
        if (siteMeshFilterDelegate != null) {
            siteMeshFilterDelegate.destroy();
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (siteMeshFilterDelegate == null) {
            //
            // Since there is no SiteMesh we are a no-op filter
            //
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        HttpServletRequest httpServletRequest = preventDoubleInvocation(servletRequest, servletResponse, filterChain);
        if (httpServletRequest == null) {
            return;
        }

        if (needsSiteMeshDecoration(httpServletRequest)) {
            //
            // we need to cause decoration to happen for this request
            //
            siteMeshFilterDelegate.doFilter(servletRequest, servletResponse, filterChain);
        } else {
            //
            // we are not to be decorated so again we become a no-op filter and pass straight through to the rest of the chain
            //
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private boolean needsSiteMeshDecoration(HttpServletRequest httpServletRequest) {
        // if SiteMesh has already run on this request then don't bother
        if (httpServletRequest.getAttribute("com.opensymphony.sitemesh.APPLIED_ONCE") != null) {
            return false;
        }

        String requestURI = makeRequestURI(httpServletRequest);
        List<SiteMeshModuleDescriptor> siteMeshModules = pluginAccessor.getEnabledModuleDescriptorsByClass(SiteMeshModuleDescriptor.class);
        for (SiteMeshModuleDescriptor module : siteMeshModules) {
            if (requestURI.startsWith(module.getPath())) {
                return true;
            }
        }
        return false;
    }

    private String makeRequestURI(HttpServletRequest httpServletRequest) {
        String requestURI = httpServletRequest.getRequestURI();
        String context = httpServletRequest.getContextPath();
        if (requestURI.startsWith(context)) {
            requestURI = requestURI.substring(context.length());
        }
        return UrlUtils.startWithSlash(requestURI);
    }


}
