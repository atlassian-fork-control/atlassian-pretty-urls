package com.atlassian.prettyurls.module;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.conditions.ConditionLoadingException;
import com.atlassian.plugin.web.descriptors.ConditionElementParser;
import com.atlassian.prettyurls.api.route.RoutePredicate;
import com.atlassian.prettyurls.api.route.RoutePredicates;
import com.atlassian.prettyurls.api.route.UrlRouteRule;
import com.atlassian.prettyurls.api.route.UrlRouteRuleSet;
import com.atlassian.prettyurls.internal.rules.UrlRouteRuleSetParser;
import com.google.common.collect.Maps;
import io.atlassian.fugue.Option;
import org.dom4j.Element;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.Callable;

import static com.atlassian.ozymandias.SafePluginPointAccess.call;
import static com.atlassian.plugin.web.baseconditions.AbstractConditionElementParser.CompositeType.AND;

/**
 * The module descriptor for URL routing
 */
public class UrlRouteModuleDescriptor extends AbstractModuleDescriptor<Object> {
    private UrlRouteRuleSet urlRouteRuleSet;
    private FilterLocation location;
    private final HostContainer hostContainer;
    private ConditionElementParser conditionElementParser;
    private Element element;


    public UrlRouteModuleDescriptor(@ComponentImport final ModuleFactory moduleFactory, final HostContainer hostContainer) {
        super(moduleFactory);
        this.hostContainer = hostContainer;
    }

    private static final Condition ALWAYS_FALSE = new Condition() {
        @Override
        public void init(final Map<String, String> params) throws PluginParseException {
        }

        @Override
        public boolean shouldDisplay(final Map<String, Object> context) {
            return false;
        }
    };

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        this.element = element;

        // gets all the standard module descriptor attributes
        super.init(plugin, element);

        location = FilterLocation.parse(element.attributeValue("location", "before-dispatch"));

        //
        // taken from the plugins web-fragment system, this parses <condition> elements
        this.conditionElementParser = new ConditionElementParser(new ConditionElementParser.ConditionFactory() {
            public Condition create(final String className, final Plugin plugin) throws ConditionLoadingException {
                // using ozymandias safety
                return call(new Callable<Condition>() {
                    @Override
                    public Condition call() throws Exception {
                        return autowire(className, plugin);
                    }
                }).getOrElse(ALWAYS_FALSE);
            }
        });
    }


    @Override
    public void enabled() {
        super.enabled();
        //
        // now get out routing information as described by them.  We can do this now because we have a host container
        // available and set etc... at enable time
        //
        urlRouteRuleSet = new UrlRouteRuleSetParser().parse(getCompleteKey(), element, location, predicateMaker(plugin, conditionElementParser));
    }


    public UrlRouteRuleSet getRuleSet() {
        return urlRouteRuleSet;
    }

    public FilterLocation getLocation() {
        return location;
    }

    @Override
    public Object getModule() {
        throw new UnsupportedOperationException("It doesn't work this way");
    }

    /**
     * We map from the underlying web fragment condition objects into routing predicates.  This allows us to present an
     * API that people can call direct and have a declarative XML one as well.
     *
     * @param plugin                 the plugin in play
     * @param conditionElementParser the underlying condition parser
     * @return a maker of route predicates
     */
    private UrlRouteRuleSetParser.PredicateMaker predicateMaker(final Plugin plugin, final ConditionElementParser conditionElementParser) {
        return new UrlRouteRuleSetParser.PredicateMaker() {
            private Option<Condition> makeCondition(final Element element) {
                // using ozymandias safety
                return call(new Callable<Condition>() {
                    @Override
                    public Condition call() throws Exception {
                        return conditionElementParser.makeConditions(plugin, element, AND);
                    }
                });
            }

            private boolean runCondition(final Option<Condition> condition, final Map<String, Object> contextMap) {
                // using ozymandias safety
                return call(new Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        return condition.get().shouldDisplay(contextMap);
                    }
                }).getOrElse(false);
            }

            @Override
            public RoutePredicate<UrlRouteRuleSet> makeRuleSetPredicate(final Element routing) {
                final Option<Condition> condition = makeCondition(routing);
                if (condition.isEmpty()) {
                    return RoutePredicates.alwaysTrue();
                } else {
                    return new RoutePredicate<UrlRouteRuleSet>() {
                        @Override
                        public boolean apply(final HttpServletRequest httpServletRequest, final UrlRouteRuleSet routeRuleSet) {
                            Map<String, Object> contextMap = Maps.newHashMap();
                            contextMap.put("request", httpServletRequest);
                            contextMap.put("routing", routeRuleSet);

                            return runCondition(condition, contextMap);
                        }
                    };
                }
            }

            @Override
            public RoutePredicate<UrlRouteRule> makeRulePredicate(final Element route) {
                final Option<Condition> condition = makeCondition(route);
                if (condition.isEmpty()) {
                    return RoutePredicates.alwaysTrue();
                } else {
                    return new RoutePredicate<UrlRouteRule>() {
                        @Override
                        public boolean apply(final HttpServletRequest httpServletRequest, final UrlRouteRule routeRule) {
                            Map<String, Object> contextMap = Maps.newHashMap();
                            contextMap.put("request", httpServletRequest);
                            contextMap.put("route", routeRule);

                            return runCondition(condition, contextMap);
                        }
                    };
                }
            }
        };
    }

    // I wish this was in the plugin system proper.  Its such a common pattern
    private <T> T autowire(final String className, final Plugin plugin) {
        try {
            Class<T> conditionClass = plugin.loadClass(className, getClass());
            if (plugin instanceof ContainerManagedPlugin) {
                return ((ContainerManagedPlugin) plugin).getContainerAccessor().createBean(conditionClass);
            } else {
                return hostContainer.create(conditionClass);
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
