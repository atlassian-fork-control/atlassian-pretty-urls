package com.atlassian.prettyurls.module;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 */
@ModuleType(ListableModuleDescriptorFactory.class)
@Component
public class SiteMeshModuleDescriptorFactory extends SingleModuleDescriptorFactory<SiteMeshModuleDescriptor> {
    @Autowired
    public SiteMeshModuleDescriptorFactory(final HostContainer hostContainer) {
        super(hostContainer, "sitemesh", SiteMeshModuleDescriptor.class);
    }
}
