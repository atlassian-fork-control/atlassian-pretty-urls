package com.atlassian.prettyurls.module;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.prettyurls.internal.util.UrlUtils;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

/**
 */
public class SiteMeshModuleDescriptor extends AbstractModuleDescriptor<Object> {

    private static final Logger log = LoggerFactory.getLogger(SiteMeshModuleDescriptor.class);

    private String path;

    public SiteMeshModuleDescriptor(@ComponentImport ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        super.init(plugin, element);
        String path = element.attributeValue("path", "").trim();
        if (path.isEmpty()) {
            log.error("You are required to have a path entry to get SiteMesh decoration.  Ignoring this module...");
        } else {
            if (path.equals("/")) {
                log.error("You cannot specify '" + path + "' as a top level path.  Ignoring this module...");
            } else {
                this.path = UrlUtils.startWithSlash(path);

            }
        }
    }

    public String getPath() {
        return path;
    }

    @Override
    public Object getModule() {
        throw new UnsupportedOperationException("Not implemented this way");
    }
}
